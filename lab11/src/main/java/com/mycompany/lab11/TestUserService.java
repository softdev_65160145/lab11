/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab11;

import com.mycompany.lab11.Service.UserService;
import com.mycompany.lab11.model.User;

/**
 *
 * @author Acer
 */
public class TestUserService {
    public static void main(String[] args) {
        UserService userService = new UserService();
        User user = userService.login("User1", "Password");
        if (user != null) {
            System.out.println("Welcome user: " + user.getName());
        }else{
            System.out.println("Error");
        }
    }
}
